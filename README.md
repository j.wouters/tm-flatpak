# tm-flatpak

Scripts to build TeXmacs as a flatpak

Plugins function only partially due to flatpak's sandboxing. The Python plugin works, using the included version of Python, not the host system's Python.

Plugins installed in the `TEXMACS_HOME_PATH` can be made to work using a wrapper script. You need to create a script in `~/.var/app/org.texmacs.TeXmacs/bin/` with the same name as the binary you wish the plugin be able to call. For example, for `julia`, create a `julia` file containing the following:
```
#!/bin/sh

flatpak-spawn --host /usr/bin/julia "$@"
```
Remember to make the file executable.

Plugins installed in `TEXMACS_PATH` that need to call host binaries probably won't function. They generally run code found in the `TEXMACS_PATH`, which should be accessible to the host binary. As the `TEXMACS_PATH` is sandboxed and this path does not normally exist in the same location on the host, this will fail.


See also:
- [gnucash-on-flatpak](https://github.com/Gnucash/gnucash-on-flatpak)
- [GnuCash wiki: Flatpak](https://wiki.gnucash.org/wiki/Flatpak)
- [Qt on Flatpak](https://docs.flatpak.org/en/latest/qt.html)
- [Flatpak-builder introduction](https://docs.flatpak.org/en/latest/building-introduction.html#flatpak-builder)
- [Denemo, uses Guile 1.8.8](https://github.com/flathub/org.denemo.Denemo)
- [Flathub build scripts](https://github.com/flathub)
- [Publish on Flathub](https://github.com/flathub/flathub/wiki/App-Submission)
- [Flatpak conventions](https://docs.flatpak.org/en/latest/conventions.html)
- [Building your first Flatpak](https://docs.flatpak.org/en/latest/first-build.html)
